<?php

namespace Drupal\lampyre;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Here we interact with the remote service.
 *
 * We use Guzzle (what else ;-) ).
 */
class LampyreApiCalls {

  /**
   * The client used to send HTTP requests.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * The headers used when sending HTTP request.
   *
   * The headers are very important when communicating with the REST server.
   * They are used by the server the verify that it supports the sent data
   * (Content-Type) and that it supports the type of response that the client
   * wants.
   *
   * @var array
   */

  protected $clientHeaders = [
    'Accept' => 'application/json',
    'Content-Type' => 'application/json',
  ];

  /**
   * The authentication token used when calling the remote REST server.
   *
   * @var string
   */

  protected $token;

  /**
   * The URL of the remote REST server.
   *
   * @var string
   */

  protected $remoteUrl;

  /**
   * The constructor.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory.
   */
  public function __construct(ClientInterface $client, ConfigFactoryInterface $config_factory) {

    $this->client = $client;
    $rest_config = $config_factory->get('lampyre.settings');
    $this->token = $rest_config->get('api_token');
    $this->remoteUrl = $rest_config->get('api_url');

  }

  /**
   * Retrieve photons balance from the remote server.
   *
   * When we retrieve photons we use GET.
   *
   * @return mixed
   *   JSON formatted string with the photons balance from the remote server.
   *
   * @throws \RuntimeException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function balance() {

    // If the configured URL is an empty string, return nothing.
    if (empty($this->remoteUrl)) {
      return '';
    }

    $response = $this->client->request('GET',
    $this->remoteUrl . '/balance?token=' . $this->token, [
      'headers' => $this->clientHeaders,
    ]
    );

    return Json::decode($response->getBody()->getContents());
  }

  /**
   * Retrieve available methods from the remote server.
   *
   * When we retrieve available methods we use GET.
   *
   * @return mixed
   *   JSON formatted string with the available methods from the remote server.
   *
   * @throws \RuntimeException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function methods() {

    // If the configured URL is an empty string, return nothing.
    if (empty($this->remoteUrl)) {
      return '';
    }

    $response = $this->client->request('GET',
      $this->remoteUrl . '/methods?token=' . $this->token, [
        'headers' => $this->clientHeaders,
      ]
    );

    return Json::decode($response->getBody()->getContents());
  }

  /**
   * Run Method By Name using POST.
   *
   * @param string $method
   *   Contains the method name to use.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A HTTP response.
   *
   * @throws \InvalidArgumentException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function runMethodByName(string $method) {

    if (empty($this->remoteUrl)) {
      return new Response('The remote endpoint URL has not been setup.', 500);
    }

    $tempstore = \Drupal::service('tempstore.private')->get('osint');
    $query_type = $tempstore->get('query_type');
    if ($query_type == 'cryptocurrency') {
      $query_type = 'cryptocurrency_wallet_address';
    }
    if ($query_type == 'company') {
      $query_type = 'company_name';
    }
    $query_text = $tempstore->get('query_text');

    $request_body = json_encode([
      'token' => $this->token,
      'task_info' => [
        $query_type => $query_text,
      ],
    ]);

    $response = $this->client->request('POST', $this->remoteUrl . '/tasks/' . $method,
      [
        'headers' => $this->clientHeaders,
        'body' => $request_body,
      ]
    );

    // Validate the response from the remote server.
    if ($response->getStatusCode() != 201) {
      return new Response('An error occurred while calling method.', 500);
    }

    $postData = Json::decode($response->getBody()->getContents());

    return $postData;

  }

  /**
   * Retrieve information for a method by task_id from the remote server.
   *
   * When we retrieve information for a task by method, we use GET.
   *
   * @return mixed
   *   JSON formatted string with the informations from the remote server.
   *
   * @throws \RuntimeException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function runMethodByNameGet(string $method, string $taskId) {

    // If the configured URL is an empty string, return nothing.
    if (empty($this->remoteUrl)) {
      return '';
    }

    $informations = $this->client->request('GET',
      $this->remoteUrl . '/tasks/' . $method . '/' . $taskId . '?token=' . $this->token, [
        'headers' => $this->clientHeaders,
      ]
    );

    return Json::decode($informations->getBody()->getContents());
  }

}
