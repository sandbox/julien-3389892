<?php

namespace Drupal\Lampyre\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Settings form for OSINT lampyre module.
 *
 * Connfigure OSINT Lampyre API.
 *
 * @ingroup Osint
 */
class LampyreSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lampyre_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['intro'] = [
      '#markup' => t('Set here the Lampyre Lighthouse API options.'),
    ];

    $form['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Lampyre API URL'),
      '#description' => $this->t('Format like this: http://www.example.com or http://www.example.com/test-site (no trailing slash)'),
      '#default_value' => $this->config('lampyre.settings')->get('api_url'),
      '#required' => TRUE,
    ];

    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Lampyre API token'),
      '#default_value' => $this->config('lampyre.settings')->get('api_token'),
      '#description' => $this->t('token to access the API in the /lighthouse/api tab in your account'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $form_values = $form_state->getValues();

    $this->config('lampyre.settings')
      ->set('api_url', $form_values['api_url'])
      ->set('api_token', $form_values['api_token'])
      ->save();

    parent::submitForm($form, $form_state);

  }

  /**
   * {@inheritdoc}osint
   */
  public function getEditableConfigNames() {
    return [
      'lampyre.settings',
    ];
  }

}
