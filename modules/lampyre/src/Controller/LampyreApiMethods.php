<?php

namespace Drupal\lampyre\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\lampyre\LampyreApiCalls;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 *
 * Controller routines for lampyre routes.
 *
 * @ingroup osint
 *
 */

class LampyreApiMethods extends ControllerBase {

  /**
   * lampureApiCalls object.
   *
   * @var \Drupal\lampyre\LampyreApiCalls
   **/

  private $lampyreApiCalls;

  /**
   * LampyreApiMethods constructor.
   *
   * @param \Drupal\lampyre\LampyreApiCalls lampyreApiCalls
   *
   */

  public function __construct(LampyreApiCalls $lampyreApiCalls) {
    $this->lampyreApiCalls = $lampyreApiCalls;
  }

  /**
   * {@inheritdoc}
   **/
  public static function create(ContainerInterface $container) {
     return new static(
       $container->get('lampyre_api_calls')
     );
   }

  /**
   * Run Method By Name route.
   *
   * @param string $method
   *   Contains the method name to use.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A HTTP response.
   *
   * @throws \InvalidArgumentException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function runMethodByName(string $method) {

    $response = $this->lampyreApiCalls->runMethodByName($method);
    $url = Url::fromRoute('lampyre.results_method_by_name', [
      'method' => $method,
      'taskid' => $response['task_id']
    ]);

    return new RedirectResponse($url->toString());

   }

  /**
   * Display Results Method By Name route.
   *
   * @param string $method
   *   Contains the method name to use.
   * @param string $taskid
   * Contains the task id to use.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A HTTP response.
   *
   * @throws \InvalidArgumentException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function resultsMethodByName(string $method, string $taskid) {

    $response = $this->lampyreApiCalls->runMethodByNameGet($method, $taskid);
    if ($response['task_status'] != 0) {
      $tempstore = \Drupal::service('tempstore.private')->get('osint');
      $methodTaskIdKeyString = hash('sha256', ($tempstore->get('query_type') . '_' . $tempstore->get('query_text') . '_' . $method));
      $tempstore->set($methodTaskIdKeyString, $taskid);
      return new RedirectResponse(Url::fromRoute('osint.search_methods', [], [])->toString(), 302);
    }

    $build = [];

    $build['intro'] = [
      '#markup' => $this->t('If there is any results, it will be displayed below.'),
    ];
    if (!empty($response['result']['result'][0]['output'])) {
      $header_columns = array_keys($response['result']['result'][0]['output'][0]);
      $rows = $response['result']['result'][0]['output'];


      $build['methods_table'] = [
        '#type' => 'table',
        '#header' => $header_columns,
        '#empty' => t('There are no results on the remote system yet'),
      ];

      if (!empty($rows)) {
        foreach ($rows as $delta => $row) {
          foreach ($header_columns as $delta2 => $header) {
            $build['methods_table'][$delta][$header_columns[$delta2]]['#plain_text'] = $row[$header_columns[$delta2]];
          }
        }
      }
    }

    return $build;

  }

}


