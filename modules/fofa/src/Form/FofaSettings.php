<?php

namespace Drupal\Fofa\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Settings form for OSINT Fofa module.
 *
 * Connfigure OSINT Fofa API.
 *
 * @ingroup Osint
 */
class FofaSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fofa_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['intro'] = [
      '#markup' => t('Set here the FOFA API options.'),
    ];

    $form['api_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FOFA API EMAIL'),
      '#description' => $this->t('Email associated with FOFA Account'),
      '#default_value' => $this->config('fofa.settings')->get('api_email'),
      '#required' => TRUE,
    ];

    $form['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FOFA API URL'),
      '#description' => $this->t('Format like this: http://www.example.com or http://www.example.com/test-site (no trailing slash)'),
      '#default_value' => $this->config('fofa.settings')->get('api_url'),
      '#required' => TRUE,
    ];

    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FOFA API token'),
      '#default_value' => $this->config('fofa.settings')->get('api_token'),
      '#description' => $this->t('token to access the API in the https://fofa.info/api/v1 tab in your account'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $form_values = $form_state->getValues();

    $this->config('fofa.settings')
      ->set('api_email', $form_values['api_email'])
      ->set('api_url', $form_values['api_url'])
      ->set('api_token', $form_values['api_token'])
      ->save();

    parent::submitForm($form, $form_state);

  }

  /**
   * {@inheritdoc}osint
   */
  public function getEditableConfigNames() {
    return [
      'fofa.settings',
    ];
  }

}
