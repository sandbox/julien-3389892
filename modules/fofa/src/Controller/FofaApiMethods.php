<?php

namespace Drupal\fofa\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\fofa\FofaApiCalls;

/**
 *
 * Controller routines for fofa routes.
 *
 * @ingroup osint
 *
 */

class FofaApiMethods extends ControllerBase {

  /**
   * fofaApiCalls object.
   *
   * @var \Drupal\fofa\FofaApiCalls
   **/

  private $fofaApiCalls;

  /**
   * FofaApiMethods constructor.
   *
   * @param \Drupal\fofa\FofaApiCalls fofaApiCalls
   *
   */

  public function __construct(FofaApiCalls $fofaApiCalls) {
    $this->fofaApiCalls = $fofaApiCalls;
  }

  /**
   * {@inheritdoc}
   **/
  public static function create(ContainerInterface $container) {
     return new static(
       $container->get('fofa_api_calls')
     );
   }

  /**
   * Display Results Method By Name route.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A HTTP response.
   *
   * @throws \InvalidArgumentException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function results() {

    $response = $this->fofaApiCalls->runSearch();
    $nodes = $response['results'];

    $build = [];

    $build['intro'] = [
      '#markup' => $this->t('If there is any results, it will be displayed below.'),
    ];

    $build['node_table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('IP'),
        $this->t('Port'),
        $this->t('Protocol'),
        $this->t('Country'),
        $this->t('Country Name'),
        $this->t('Region'),
        $this->t('City'),
        $this->t('Longitude'),
        $this->t('Latitude'),
        $this->t('ASN Number'),
        $this->t('ASN Organization'),
        $this->t('Host'),
        $this->t('Domain'),
        $this->t('OS'),
        $this->t('Server'),
        $this->t('ICP Number Information'),
        //$this->t('Website Title'),
        $this->t('JARM Fingerprint'),
        //$this->t('Type is subdomain is header'),
        //$this->t('Type is service is banner'),
        //$this->t('Cert'),
        $this->t('Base protocol,for example:tcp/udpe'),
        //$this->t('Asset URL'),
        //$this->t('SSL issuer organization'),
        //$this->t('SSL issuer common name'),
        //$this->t('SSL subject organization'),
        //$this->t('SSL subject common name')
      ],
      '#empty' => t('There are no items on the remote system yet'),
    ];

    if (!empty($nodes)) {
      foreach ($nodes as $delta => $node) {
        $build['node_table'][$delta]['ip']['#plain_text'] = $node[0];
        $build['node_table'][$delta]['port']['#plain_text'] = $node[1];
        $build['node_table'][$delta]['protocol']['#plain_text'] = $node[2];
        $build['node_table'][$delta]['country']['#plain_text'] = $node[3];
        $build['node_table'][$delta]['country_name']['#plain_text'] = $node[4];
        $build['node_table'][$delta]['region']['#plain_text'] = $node[5];
        $build['node_table'][$delta]['city']['#plain_text'] = $node[6];
        $build['node_table'][$delta]['longitude']['#plain_text'] = $node[7];
        $build['node_table'][$delta]['latitude']['#plain_text'] = $node[8];
        $build['node_table'][$delta]['asn_number']['#plain_text'] = $node[9];
        $build['node_table'][$delta]['asn_organization']['#plain_text'] = $node[10];
        $build['node_table'][$delta]['host']['#plain_text'] = $node[11];
        $build['node_table'][$delta]['domain']['#plain_text'] = $node[12];
        $build['node_table'][$delta]['os']['#plain_text'] = $node[13];
        $build['node_table'][$delta]['server']['#plain_text'] = $node[14];
        $build['node_table'][$delta]['icp']['#plain_text'] = $node[15];
        //$build['node_table'][$delta]['website_title']['#plain_text'] = $node[16];
        $build['node_table'][$delta]['jarm']['#plain_text'] = $node[17];
        //$build['node_table'][$delta]['header']['#plain_text'] = $node[18];
        //$build['node_table'][$delta]['banner']['#plain_text'] = $node[19];
        //$build['node_table'][$delta]['cert']['#plain_text'] = $node[18];
        $build['node_table'][$delta]['baseprotocol']['#plain_text'] = $node[19];
        /*$build['node_table'][$delta]['asseturl']['#plain_text'] = $node[20];
        $build['node_table'][$delta]['sslorganization']['#plain_text'] = $node[21];
        $build['node_table'][$delta]['sslcommonname']['#plain_text'] = $node[22];
        $build['node_table'][$delta]['sslsubjectorganization']['#plain_text'] = $node[23];
        $build['node_table'][$delta]['sslsubjectcommonname']['#plain_text'] = $node[24];*/

      }
    }

    return $build;

  }

}


