<?php

namespace Drupal\fofa;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Here we interact with the remote service.
 *
 * We use Guzzle (what else ;-) ).
 */
class FofaApiCalls {

  /**
   * The client used to send HTTP requests.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * The headers used when sending HTTP request.
   *
   * The headers are very important when communicating with the REST server.
   * They are used by the server the verify that it supports the sent data
   * (Content-Type) and that it supports the type of response that the client
   * wants.
   *
   * @var array
   */

  protected $clientHeaders = [
    'Accept' => 'application/json',
    'Content-Type' => 'application/json',
  ];

  /**
   * The email associated to FOFA account on the remote REST server.
   *
   * @var string
   */

  protected $email;

  /**
   * The authentication token used when calling the remote REST server.
   *
   * @var string
   */

  protected $token;

  /**
   * The URL of the remote REST server.
   *
   * @var string
   */

  protected $remoteUrl;

  /**
   * The constructor.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory.
   */
  public function __construct(ClientInterface $client, ConfigFactoryInterface $config_factory) {

    $this->client = $client;
    $rest_config = $config_factory->get('fofa.settings');
    $this->email = $rest_config->get('api_email');
    $this->token = $rest_config->get('api_token');
    $this->remoteUrl = $rest_config->get('api_url');

  }

  /**
   * Retrieve available methods from the remote server.
   *
   * When we retrieve available methods we use GET.
   *
   * @return mixed
   *   JSON formatted string with the available methods from the remote server.
   *
   * @throws \RuntimeException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function runSearch() {

    // If the configured URL is an empty string, return nothing.
    if (empty($this->remoteUrl)) {
      return '';
    }
    $tempstore = \Drupal::service('tempstore.private')->get('osint');
    $query_type = $tempstore->get('query_type');
    $query_text = $tempstore->get('query_text');
    $qbase64 = base64_encode($query_type . '=' . $query_text);
    $response = $this->client->request('GET',
      $this->remoteUrl . '/search/all?email=' . $this->email . '&key=' . $this->token . '&qbase64=' . $qbase64 . '&fields=ip,port,protocol,country,country_name,region,city,longitude,latitude,as_number,as_organization,host,domain,os,server,icp,title,jarm,cert,base_protocol,link,certs_issuer_org,certs_issuer_cn,certs_subject_org, certs_subject_cn', [
        'headers' => $this->clientHeaders,
      ]
    );

    return Json::decode($response->getBody()->getContents());
  }

}
