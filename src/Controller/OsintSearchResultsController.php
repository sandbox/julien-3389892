<?php

namespace Drupal\osint\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\osint\Utility\OsintDescriptionTemplateTrait;

/**
 * Provides a help page for the OSINT module.
 *
 * @ingroup osint
 */
class OsintSearchResultsController extends ControllerBase {

  use OsintDescriptionTemplateTrait;

  /**
   * {@inheritdoc}
   */
  protected function getModuleName() {
    return 'osint';
  }

}
