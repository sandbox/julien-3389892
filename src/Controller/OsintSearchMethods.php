<?php

namespace Drupal\osint\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\lampyre\LampyreApiCalls;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * Controller routines for osint routes.
 *
 * @ingroup osint
 *
 */

class OsintSearchMethods extends ControllerBase {

  /**
   * lampureApiCalls object.
   *
   * @var \Drupal\lampyre\LampyreApiCalls
   **/

  private $lampyreApiCalls;

  /**
   * OsintSearchMethods constructor.
   *
   * @param \Drupal\lampyre\LampyreApiCalls lampyreApiCalls
   *
   * RestExampleClientCalls service.
   */

  public function __construct(LampyreApiCalls $lampyreApiCalls) {
    $this->lampyreApiCalls = $lampyreApiCalls;
  }

  /**
   * {@inheritdoc}
   **/
  public static function create(ContainerInterface $container) {
     return new static(
       $container->get('lampyre_api_calls')
     );
   }

   /**
    * Retrieve a list of all methods available on the remote site.
    * Building the list as a table by calling the RestExampleClientCalls::index()
    * and builds the list from the response of that.
    *
    * @throws \RuntimeException
    * @throws \GuzzleHttp\Exception\GuzzleException
    **/
   public function methodsList() {

     $tempstore = \Drupal::service('tempstore.private')->get('osint');
     $api = $tempstore->get('api');
     $query_type = $tempstore->get('query_type');

     $build = [];
     $rows = [];

     if ($api == 'lampyre') {
       $methods = $this->lampyreApiCalls->methods();
       if (!empty($methods)) {
         foreach ($methods as $delta => $method) {
           if (str_starts_with($method['method_name'], $query_type . '_')) {
             $rows[]['method_name'] = $method['method_name'];
           }
         }
       }
     }

     $build['intro'] = [
       '#markup' => $this->t('This is a list of methods on the remote server. From here you can select methods to call.'),
     ];

     $build['methods_table'] = [
       '#type' => 'table',
       '#header' => [
         $this->t('Method name'),
         $this->t('Run'),
         $this->t('View results'),
         $this->t('Import results')
       ],
       '#empty' => t('There are no methods available on the remote system yet'),
     ];

     if (!empty($rows)) {
       foreach ($rows as $delta => $row) {
         $methodTaskIdKeyString = hash('sha256', ($tempstore->get('query_type') . '_' . $tempstore->get('query_text') . '_' . $row['method_name']));
         $rowTaskId = $tempstore->get($methodTaskIdKeyString);

         $build['methods_table'][$delta]['method_name']['#plain_text'] = $row['method_name'];
         $build['methods_table'][$delta]['run']['#markup'] = Link::createFromRoute($this->t('Run'), 'lampyre.run_method_by_name', ['method' => $row['method_name']])->toString();
         $build['methods_table'][$delta]['results']['#markup'] = !is_null($rowTaskId) ? Link::createFromRoute($this->t('View Results'), 'lampyre.results_method_by_name', ['method' => $row['method_name'], 'taskid' => $rowTaskId])->toString() : '';
       }
     }

     return $build;
   }

}


