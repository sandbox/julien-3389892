<?php

namespace Drupal\osint\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a search form with multiple steps.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class SearchForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'osint_search_multistep_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    if ($form_state->has('page_num') && $form_state->get('page_num') == 2) {
      return $this->osintSearchFormPageTwo($form, $form_state);
    }

    $form_state->set('page_num', 1);

    $form['description'] = [
      '#type' => 'item',
      '#title' => $this->t('Select which API to use'),
    ];

    $moduleHandler = \Drupal::service('module_handler');
    $apis = [];

    if ($moduleHandler->moduleExists('lampyre')){
      $apis['lampyre'] = $this->t('Lampyre');
    }

    if ($moduleHandler->moduleExists('fofa')){
      $apis['fofa'] = $this->t('FOFA');
    }

    $form['api'] = [
      '#type' => 'select',
      '#title' => $this->t('API to use'),
      '#options' => $apis,
      '#empty_option' => $this->t('-select-'),
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['next'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Next'),
      // Custom submission handler for page 1.
      '#submit' => ['::osintSearchFormNextSubmit'],
      // Custom validation handler for page 1.
      '#validate' => ['::osintSearchFormNextValidate'],
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $page_values = $form_state->get('page_values');

    $this->messenger()->addMessage($this->t('The form has been submitted. api="@api", query_text="@query_text", query_type="@query_type"', [
      '@api' => $page_values['api'],
      '@query_text' => $form_state->getValue('query_text'),
      '@query_type' => $form_state->getValue('query_type'),
    ]));

    $tempstore = \Drupal::service('tempstore.private')->get('osint');
    $tempstore->set('api', $page_values['api']);
    $tempstore->set('query_type', $form_state->getValue('query_type'));
    $tempstore->set('query_text', $form_state->getValue('query_text'));

    if ($page_values['api'] == 'lampyre') {
      $form_state->setRedirectUrl(Url::fromUri('internal:/' . 'osint/search/methods'));
    }

    if ($page_values['api'] == 'fofa') {
      $form_state->setRedirectUrl(Url::fromUri('internal:/' . 'osint/search/fofa'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $query_type = $form_state->getValue('query_type');
    $query_text = $form_state->getValue('query_text');

    if ($query_type == 'email') {
      if (\Drupal::service('email.validator')->isValid($query_text) === FALSE) {
        $form_state->setErrorByName('query_text', $this->t('Please enter a valid email.'));
      }
    }
    if ($query_type == 'phone') {
      if (filter_var($query_text, FILTER_SANITIZE_NUMBER_INT) == '') {
        $form_state->setErrorByName('query_text', $this->t('Please enter a valid international phone number.'));
      }
    }
    if ($query_type == 'domain') {
      if (filter_var(gethostbyname($query_text), FILTER_VALIDATE_IP) == '') {
        $form_state->setErrorByName('query_text', $this->t('Please enter a valid domain.'));
      }
    }

  }

  /**
   * Provides custom validation handler for page 1.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function osintSearchFormNextValidate(array &$form, FormStateInterface $form_state) {
    /*$birth_year = $form_state->getValue('birth_year');

    if ($birth_year != '' && ($birth_year < 1900 || $birth_year > 2000)) {
      // Set an error for the form element with a key of "birth_year".
      $form_state->setErrorByName('birth_year', $this->t('Enter a year between 1900 and 2000.'));
    }*/
  }

  /**
   * Provides custom validation handler for page 2.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function osintSearchFormNextValidatePage2(array &$form, FormStateInterface $form_state) {
    /*$birth_year = $form_state->getValue('birth_year');

    if ($birth_year != '' && ($birth_year < 1900 || $birth_year > 2000)) {
      // Set an error for the form element with a key of "birth_year".
      $form_state->setErrorByName('birth_year', $this->t('Enter a year between 1900 and 2000.'));
    }*/
  }

  /**
   * Provides custom submission handler for page 1.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function osintSearchFormNextSubmit(array &$form, FormStateInterface $form_state) {
    $form_state
      ->set('page_values', [
        // Keep only first step values to minimize stored data.
        'api' => $form_state->getValue('api'),
      ])
      ->set('page_num', 2)
      // Since we have logic in our buildForm() method, we have to tell the form
      // builder to rebuild the form. Otherwise, even though we set 'page_num'
      // to 2, the AJAX-rendered form will still show page 1.
      ->setRebuild(TRUE);
  }

  /**
   * Builds the second step form (page 2).
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function osintSearchFormPageTwo(array &$form, FormStateInterface $form_state) {

    if ($form_state->getValue('api') == 'lampyre') {
      $form['description'] = [
        '#type' => 'item',
        '#title' => $this->t('Select the type of lampyre API parameter to use and the parameter value.'),
      ];

      $form['query_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Query Type'),
        '#options' => [
          'email' => $this->t('Email'),
          'phone' => $this->t('Phone'),
          'username' => $this->t('Username'),
          'person' => $this->t('Person'),
          'domain' => $this->t('Domain'),
          'company' => $this->t('Company'),
          'imei' => $this->t('Imei'),
          'cryptocurrency' => $this->t('Cryptocurrency')
        ],
        '#empty_option' => $this->t('-select-'),
        '#required' => TRUE,
      ];

      $form['query_text'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Input parameter'),
        '#default_value' => $form_state->getValue('query_text', ''),
        '#description' => $this->t('Enter input parameter.'),
        // Custom validation handler for page 2.
        '#validate' => ['::osintSearchFormNextValidatePage2'],
      ];
    }

    if ($form_state->getValue('api') == 'fofa') {

      $form['description'] = [
        '#type' => 'item',
        '#title' => $this->t('Select the type of FOFA API parameter to use and the parameter value.'),
      ];

      $form['query_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Query Type'),
        '#options' => [
          'title' => $this->t('Title'),
          'domain' => $this->t('Domain')
        ],
        '#empty_option' => $this->t('-select-'),
        '#required' => TRUE,
      ];

      $form['query_text'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Query text'),
        '#default_value' => $form_state->getValue('query_text', ''),
        '#description' => $this->t('Enter input parameter.'),
        // Custom validation handler for page 2.
        '#validate' => ['::osintSearchFormNextValidatePage2'],
      ];
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['back'] = [
      '#type' => 'submit',
      '#value' => $this->t('Back'),
      // Custom submission handler for 'Back' button.
      '#submit' => ['::osintSearchFormPageTwoBack'],
      '#limit_validation_errors' => [],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Submit'),
    ];

    return $form;

  }

  /**
   * Provides custom submission handler for 'Back' button (page 2).
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function osintSearchFormPageTwoBack(array &$form, FormStateInterface $form_state) {

    $form_state
      // Restore values for the first step.
      ->setValues($form_state->get('page_values'))
      ->set('page_num', 1)
      // Since we have logic in our buildForm() method, we have to tell the form
      // builder to rebuild the form. Otherwise, even though we set 'page_num'
      // to 1, the AJAX-rendered form will still show page 2.
      ->setRebuild(TRUE);

  }

}
