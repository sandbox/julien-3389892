<?php

namespace Drupal\Osint\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Settings form for Osint module.
 *
 * Connfigure Osint.
 *
 * @ingroup Osint
 */
class OsintSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'osint_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['intro'] = [
      '#markup' => t('Set here OSINT options.'),
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $form_values = $form_state->getValues();

    parent::submitForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      'osint.settings',
    ];
  }

}
